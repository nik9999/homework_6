﻿

using System.Collections;

Console.WriteLine("Test Extension with delegate!");

Func<object, float> _getParameter = flt => (float)flt;

List<object> floatList = new List<object>() { 1.1f, 2.1f, 3.4f, 1.4f, 22.4f };
var max = floatList.GetMax(_getParameter);

Console.WriteLine(max);
Console.ReadLine();


static class EnumExtentions
{
    public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
    {
        float res = float.MinValue;

        foreach (T value in e)
        {
            if (getParameter(value) > res)
                res = getParameter(value);
        }

        return (T)Convert.ChangeType(res, typeof(T)); 
    }
   
}