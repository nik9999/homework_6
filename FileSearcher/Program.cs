﻿using FileSearcher;

int i = 0;
Console.WriteLine("Start search first 10 files !");

FileLister fl = new FileLister();

fl.FileFound += onFileFound;

fl.List(@"C:\", @"*.*");

fl.FileFound -= onFileFound;

void onFileFound(object? sender, FileFoundArgs e)
{
    Console.WriteLine(e.FoundFile);

    if (i == 9)
        e.CancelRequested = true;
}